package com.clougence.cloudcanal.sdk.demos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.clougence.cloudcanal.sdk.api.CloudCanalProcessor;
import com.clougence.cloudcanal.sdk.api.constant.rdb.RecordAction;
import com.clougence.cloudcanal.sdk.api.contextkey.RdbContextKey;
import com.clougence.cloudcanal.sdk.api.metakey.RdbMetaKey;
import com.clougence.cloudcanal.sdk.api.model.CustomField;
import com.clougence.cloudcanal.sdk.api.model.CustomProcessorContext;
import com.clougence.cloudcanal.sdk.api.model.CustomRecord;
import com.clougence.cloudcanal.sdk.api.service.impl.RecordBuilder;
import lombok.SneakyThrows;

/**
 * @author wanshao create time is 2021/8/18
 **/
public class MyProcessor implements CloudCanalProcessor {

    private static final String LOG_NAME     = "custom_processor";
    private static final Logger customLogger = LoggerFactory.getLogger(LOG_NAME);

    /**
     * <pre>
     *     BEST PRACTICE
     *     Do one operation to one record. Because multi operation need pay attention to the order. Delete operation
     *     is better to process at the last to avoid other operation change the ops flag
     *
     *     TIPS:
     *     1. processor is before mapping ,using source schema info
     *     2. Now only support String value
     * </pre>
     */
    @SneakyThrows
    @Override
    public List<CustomRecord> process(List<CustomRecord> customRecordList, CustomProcessorContext customProcessorContext) {
        // query example
        tryQuerySourceDs((DataSource) customProcessorContext.getProcessorContextMap().get(RdbContextKey.SOURCE_DATASOURCE));
        // DO UPDATE_ROW_FLAG first
        customRecordList = addField(customRecordList);
        customRecordList = updateField(customRecordList);
        customRecordList = deleteField(customRecordList);
        // DO ADD ROW SECOND
        boolean bizCondition = false;
        if (bizCondition) {
            customRecordList = addRecord(customRecordList);
            Thread.sleep(1000);
            customRecordList = addRecord(customRecordList);
        }
        // DO DELETE ROW AT THE LAST
        customRecordList = deleteRecord(customRecordList);
        return customRecordList;
    }

    /**
     * Tips: The datasource is cached in CloudCanal. So no need to close it, it will be reused.
     *
     * @param dataSource
     */
    @SneakyThrows
    private void tryQuerySourceDs(DataSource dataSource) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement ps = connection.prepareStatement("select * from integration_src.student limit 1");
                ResultSet resultSet = ps.executeQuery()) {
            while (resultSet.next()) {
                customLogger.info("#### Student name  is " + resultSet.getString("name"));
            }
        }
    }

    /**
     * Add a new row
     */
    private List<CustomRecord> addRecord(List<CustomRecord> customRecordList) {
        Map<String, Object> addFieldValueMap = new LinkedHashMap<>();

        addFieldValueMap.put("id", System.currentTimeMillis() / 1000);
        addFieldValueMap.put("score", 100);
        addFieldValueMap.put("col_varchar", "new_varchar_value");
        addFieldValueMap.put("col_int", 9999);
        CustomRecord addedRecord = RecordBuilder.createRecordBuilder().createRecord(addFieldValueMap).build();
        customRecordList.add(addedRecord);
        return customRecordList;
    }

    /**
     * Delete a existed record
     */
    private List<CustomRecord> deleteRecord(List<CustomRecord> customRecordList) {
        // coordination null means useradd record
        for (CustomRecord customRecord : customRecordList) {
            if ("student".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.TABLE_NAME))
                && "integration_src".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.SCHEMA_NAME))
                && RecordAction.INSERT.equals(customRecord.getRecordMetaMap().get(RdbMetaKey.ACTION_NAME))) {
                customLogger.info("############# CAN DO SOMETHING HERE #####");

                CustomField scoreField = customRecord.getFieldMapAfter().get("score");
                // delete row that smaller than 60
                if (scoreField != null && Integer.valueOf(String.valueOf(scoreField.getValue())) < 60) {
                    RecordBuilder.modifyRecordBuilder(customRecord).deleteRecord();
                }

            }
        }
        return customRecordList;
    }

    // =========================== All below examples are update row operation =========================
    private List<CustomRecord> updateField(List<CustomRecord> customRecordList) {
        for (CustomRecord customRecord : customRecordList) {
            // set flag before process
            if ("student".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.TABLE_NAME))
                && "integration_src".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.SCHEMA_NAME))
                && RecordAction.INSERT.equals(customRecord.getRecordMetaMap().get(RdbMetaKey.ACTION_NAME))) {
                customLogger.info("############# CAN DO SOMETHING HERE #####");
                CustomField scoreField = customRecord.getFieldMapAfter().get("score");
                if (scoreField != null && "0".equals(scoreField.getValue())) {
                    Map<String, Object> updateFieldValueMap = new LinkedHashMap<>();
                    updateFieldValueMap.put("score", 99);
                    RecordBuilder.modifyRecordBuilder(customRecord).updateField(updateFieldValueMap).build();
                }
            }
        }
        return customRecordList;
    }

    private List<CustomRecord> deleteField(List<CustomRecord> customRecordList) {
        for (CustomRecord customRecord : customRecordList) {
            if ("student".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.TABLE_NAME))
                && "integration_src".equals(customRecord.getRecordMetaMap().get(RdbMetaKey.SCHEMA_NAME))
                && RecordAction.INSERT.equals(customRecord.getRecordMetaMap().get(RdbMetaKey.ACTION_NAME))) {
                customLogger.info("############# CAN DO SOMETHING HERE #####");

                // all rows belong to same table and action need to drop this field as well
                RecordBuilder.modifyRecordBuilder(customRecord).dropField("name").build();

            }
        }
        return customRecordList;
    }

    /**
     * <pre>
     *     ATTENTION!! If you use add field, then all record need add field, otherwise CloudCanal will not write into target correctly
     * </pre>
     *
     * @param customRecordList
     * @return
     */
    private List<CustomRecord> addField(List<CustomRecord> customRecordList) {
        for (CustomRecord customRecord : customRecordList) {
            Map<String, Object> addFieldValueMap = new LinkedHashMap<>();
            addFieldValueMap.put("col_varchar", null);
            addFieldValueMap.put("col_int", 9999);
            RecordBuilder.modifyRecordBuilder(customRecord).addField(addFieldValueMap);
        }
        return customRecordList;
    }
}
